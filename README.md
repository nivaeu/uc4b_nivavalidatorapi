# Introduction
This subproject is part of the ["New IACS Vision in Action” NIVA](https://www.niva4cap.eu/) project that delivers 
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to 
support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under 
grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects 
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# NivaValidatorApi
NivaValidatorApi is an API for validating machine data. This repository contains a proof of concept developed at SEGES (https://seges.dk/)

## Purpose
The purpose of nivavalidatorapi is validating of ECropMessages. Validation includes schema and business validation.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
Necessary software to use this repository

- **Visual Studio 2019:** https://visualstudio.microsoft.com/downloads/
- **.NET Core 3.1:** https://dotnet.microsoft.com/download/dotnet-core/3.1
- **git:** https://git-scm.com/downloads

## Installing
Clone the repository

Windows example:
```
In console or with powershell write this:
git clone https://gitlab.com/nivaeu/uc4b_nivavalidatorapi.git 
```

## License
EU-PL Nest framework is <a rel="nofollow noreferrer noopener" href="https://github.com/nestjs/nest/blob/master/LICENSE">MIT licensed</a>.

![European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009](NIVA.jpg)
