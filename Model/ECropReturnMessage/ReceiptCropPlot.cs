﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class ReceiptCropPlot
	{
		/// <summary>
		/// Year
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("Year")]
		public int Year { get; set; }

		/// <summary>
		/// FieldNumber
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("FieldNumber")]
		public string FieldNumber { get; set; }

		/// <summary>
		/// LegislationCrop
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("LegislationCrop")]
		public string[] LegislationCrop { get; set; }
	}

    public class ReceiptCropPlotSmn : ReceiptCropPlot
	{
		/// <summary>
		/// Produce
		/// SMN
		/// </summary>
		[JsonProperty]
		public string[] Produce { get; set; }

		/// <summary>
		/// Statistics values for task
		/// SMN
		/// </summary>
		[JsonProperty]
		public DescriptiveStatistics DescriptiveStatistics { get; set; }

		/// <summary>
		/// AppliedList
		/// </summary>
		[JsonProperty]
		public ReceiptApplied[] AppliedList { get; set; }
	}

	public class ReceiptCropPlotScc : ReceiptCropPlot
	{
		/// <summary>
		/// CropConstituents
		/// SCC
		/// </summary>
		[JsonProperty]
		public string[] CropConstituents { get; set; }

		/// <summary>
		/// AppliedList
		/// </summary>
		[JsonProperty]
		public ReceiptApplied[] AppliedList { get; set; }
    }

	public class DescriptiveStatistics
	{
		/// <summary>
		/// Calculate the population mean
		/// </summary>
		public StatisticValue Mean { get; set; }
		/// <summary>
		/// Calculate the coefficient of variation 
		/// </summary>
		public StatisticValue CoefficientVariation { get; set; }
		/// <summary>
		/// Minimum value
		/// </summary>
		public StatisticValue Minimum { get; set; }
		/// <summary>
		/// Maximum value
		/// </summary>
		public StatisticValue Maximum { get; set; }
	}

	public class StatisticValue
	{
		/// <summary>
		/// Value
		/// </summary>
		public double Value { get; set; }

		/// <summary>
		/// Unit
		/// </summary>
		public string Unit { get; set; }
	}
}
