﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Linq;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class MessageDto
	{
		public MessageDto(string propertyName, MessageCode messageCode, string severity)
		{
			Element = string.Join("", propertyName.Split(".").ToList().Select(x => $"[\'{x}\']"));
			MessageCode = (int)messageCode;
			Severity = severity;
			Message = MapMessage(messageCode);
		}
		/// <summary>
		/// Refers to the element where the error occurred
		/// </summary>
		[JsonProperty("Element")]
		public string Element { get; }

		/// <summary>
		/// Can be either 'Debug', 'Info', 'Warning', 'Error' - for now only 'Error' will be used
		/// </summary>
		[JsonProperty("Severity")]
		public string Severity { get; }

		/// <summary>
		/// Code uniquely identifying the error. Please log errorcodes in document on Sharepoint
		/// </summary>
		[JsonProperty("MessageCode")]
		public int MessageCode { get; }

		/// <summary>
		/// Human readable error message in English - FMIS's can return a localized version based on the ErrorCode
		/// </summary>
		[JsonProperty("Message")]
		public string Message { get; }

		private static string MapMessage(MessageCode messageCode)
		{
			switch (messageCode)
			{
				case ECropReturnMessage.MessageCode.None:
					return "Unknown";
				case ECropReturnMessage.MessageCode.MandatoryField:
					return "Mandatory field";
				case ECropReturnMessage.MessageCode.NullField:
					return "Field is null";
				case ECropReturnMessage.MessageCode.CodeNotSupported:
					return "Code not supported";
				case ECropReturnMessage.MessageCode.OrganisationSpecificCodeNotSupported:
					return "Organisation specific code not supported";
				case ECropReturnMessage.MessageCode.TwoOrMoreConstituents:
					return "Two or more constituents required";
				case ECropReturnMessage.MessageCode.DateCanNotBeFuture:
					return "Date can not be future";
				case ECropReturnMessage.MessageCode.DateTimeFormatNotValid:
					return "DateTime format not valid";
				case ECropReturnMessage.MessageCode.ClassificationCodeNotSupported:
					return "Classification code not supported";
				case ECropReturnMessage.MessageCode.SpecifiedProductBatchIdNotSupported:
					return "Specified product batch id not supported";
				default:
					return $"MessageCode not implemented, {messageCode}";
			}
		}
	}
}