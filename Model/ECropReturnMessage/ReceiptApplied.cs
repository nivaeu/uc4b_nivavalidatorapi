﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	public class ReceiptApplied
	{
		/// <summary>
		/// Area applied total
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("AreaAppliedTotal")]
		public double AreaAppliedTotal { get; set; }

		/// <summary>
		/// Average application rate
		/// SCC = seed
		/// SMN = fertilizer
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("AverageApplicationRate")]
		public double AverageApplicationRate { get; set; }

		/// <summary>
		/// ApplicationDate
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("ApplicationDate")]
		public DateTime ApplicationDate { get; set; }

		/// <summary>
		/// Max Application Rate
		/// SCC = seed
		/// SMN = fertilizer
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("MaxApplicationRate")]
		public double MaxApplicationRate { get; set; }

		/// <summary>
		/// Min Application Rate
		/// SCC = seed
		/// SMN = fertilizer
		/// </summary>
		[JsonProperty]
		[JsonPropertyName("MinApplicationRate")]
		public double MinApplicationRate { get; set; }
	}
}
