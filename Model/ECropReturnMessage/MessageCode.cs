﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaValidatorApi.Model.ECropReturnMessage
{
	/// <summary>
	/// None = 0
	/// MandatoryField = 101
	///	NullField=102
	/// CodeNotSupported=103,
	/// OrganisationSpecificCodeNotSupported=104,
	/// TwoOrMoreConstituents=105,
	/// DateCanNotBeFuture=106
	/// DateTimeFormatNotValid=107
	/// </summary>
	public enum MessageCode
	{
		None = 0,
		MandatoryField = 101,
		NullField=102,
		CodeNotSupported=103,
		OrganisationSpecificCodeNotSupported=104,
		TwoOrMoreConstituents=105,
		DateCanNotBeFuture=106,
		DateTimeFormatNotValid=107,
		ClassificationCodeNotSupported=108,
		SpecifiedProductBatchIdNotSupported=109

	}
}
