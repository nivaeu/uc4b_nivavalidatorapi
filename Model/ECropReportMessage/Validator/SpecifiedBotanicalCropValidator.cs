﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedBotanicalCropValidator :AbstractValidator<SpecifiedBotanicalCrop>
	{
        public SpecifiedBotanicalCropValidator(OrganisationCode organisationCode)
		{
			RuleFor(x => x.BotanicalIdentificationId)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.Must(id => m_BotanicalIdentificationCodes.ContainsKey(organisationCode) == false || m_BotanicalIdentificationCodes[organisationCode].Contains(id))
				.WithState(x2 => MessageCode.OrganisationSpecificCodeNotSupported);
		}

        private readonly Dictionary<OrganisationCode, List<string>> m_BotanicalIdentificationCodes = new Dictionary<OrganisationCode, List<string>>()
        {
            /*
			 * 5707 Oil radish
			 * 5711 Græsmarksplant./Grass field species
			 * 5765 Egyptian clover
			 * 5799 Vårhvede/Spring wheat
			 * 5793 Vinterhvede/Winter wheat
			 * 5795 Alm.rajg., m.tidl, græsm./Perennial ryegrass, m.early, forage
			 * 1673138 Efterafgrøder/Catch crops
			 * 1674099 Phacelia
			 */
            { OrganisationCode.DK,new List<string>(){ "5707", "5711", "5765", "5799", "5793","5795","1673138", "1674099"}},
            { OrganisationCode.NL,new List<string>(){ "5707", "5711", "5765", "5799", "5793","5795","1673138", "1674099"}}
        };
    }
}
