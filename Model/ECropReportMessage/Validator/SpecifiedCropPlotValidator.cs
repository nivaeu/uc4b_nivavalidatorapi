﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedCropPlotValidator : AbstractValidator<SpecifiedCropPlot>
	{
		public SpecifiedCropPlotValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.GrownFieldCrop)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(crop => crop.SetValidator(new GrownFieldCropValidator(organisationCode, reportTypeCode)));
			RuleFor(x => x.SpecifiedReferencedLocation)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new ReferencedLocationValidator());
			RuleFor(x => x.StartDateTime)
				.SetValidator(new DateTimeValidator());
			RuleFor(x => x.EndDateTime)
				.SetValidator(new DateTimeValidator());
		}
	}
}
