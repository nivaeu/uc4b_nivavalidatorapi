﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Linq;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedProductBatchValidator : AbstractValidator<SpecifiedProductBatch>
	{
        /*
		 * 1195006 (DEV, landsnorm SEGES, used for actual spanish farm) (ed. 05-07-2022)
		 * 1195068 NS 26-15
		 * 1195256 NPKS 22- 2-13- 3
		 * 1682707 Kalkammonsalpeter 27 N (PROD, centernormsæt SEGES English norms, used for actual NL farm demo Niva Durch Farm) (ed. 28-05-2020
		 * 1682276 Kalkammonsalpeter 27 N (DEV, centernormsæt SEGES English norms, used for internal testfarm Niva Project Testfarm) (ed. 28-05-2020)
		 * 1195069 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 * 1671574 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 * 1195041 NS 27-4 * (PROD, landsnorm SEGES, used for actual DK farm) (ed. 13-08-2020)
		 * 1765933 NPK 16-16-16 (PROD, landsnorm SEGES, used for actual Greece farm) (ed. 30-06-2021)
		 * 1765637 NPK 16-16-16 (DEV, landsnorm SEGES, used for actual Greece farm) (ed. 30-06-2022)
		 * 1195067 NS 26-13 (13S) (DEV, landsnorm SEGES, used for actual spanish farm) (ed. 06-07-2022) 
		 * 1767745 Latvian prod (ed. 23-08-2022)
		 * 1765639 Latvian dev (ed. 24-08-2022)
		 *
	    */
        static string[] ValidCommercialFertilizerIds = new string[] {"1195006", "1195068", "1195256", "1682707", "1682276", "1195069", "1671574", "1195041", "1765933", "1765637", "1195067", "1767745", "1765639" };

		public SpecifiedProductBatchValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			if (reportTypeCode == ReportTypeCode.SMN)
			{
				RuleFor(x => x.Id)
					.NotNull()
					.WithState(x1 => MessageCode.NullField)
					.Must(x2 => ValidCommercialFertilizerIds.Contains(x2))
					.WithState(x3 => MessageCode.SpecifiedProductBatchIdNotSupported);
			}
		}
	}
}
