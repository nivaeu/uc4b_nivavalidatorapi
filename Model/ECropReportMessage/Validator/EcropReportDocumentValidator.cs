﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class EcropReportDocumentValidator : AbstractValidator<CropReportDocument>
	{
		private readonly List<string> m_SupportedTypeCodes = new List<string>() {"SCC", "SMN"};
		public EcropReportDocumentValidator()
		{
			RuleFor(x => x.TypeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField)
				.Must(typeCode => m_SupportedTypeCodes.Contains(typeCode))
				.WithState(x1 => MessageCode.CodeNotSupported);
			RuleFor(x => x.PurposeCode)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.SenderSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
			RuleFor(x => x.RecipientSpecifiedParty)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.SetValidator(new SpecifiedPartyValidator());
			RuleFor(x => x.IssueDateTime)
				.SetValidator(new DateTimeValidator());
		}
	}
}