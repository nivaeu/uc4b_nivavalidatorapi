﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class DateTimeValidator : AbstractValidator<string>
	{
		public DateTimeValidator()
		{
			RuleFor(x => x)
				.Must(x => DateTime.TryParse(x, out _) == true)
				.WithState(x => MessageCode.DateTimeFormatNotValid);
		}
	}
}