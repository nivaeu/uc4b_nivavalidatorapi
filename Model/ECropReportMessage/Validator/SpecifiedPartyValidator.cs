﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class SpecifiedPartyValidator : AbstractValidator<SpecifiedParty>
	{
		public SpecifiedPartyValidator()
		{
			RuleFor(x => x.Id)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.CountryId)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
			RuleFor(x => x.Name)
				.Must(purpose => !string.IsNullOrEmpty(purpose))
				.WithState(x1 => MessageCode.MandatoryField);
		}
	}
}
