﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/


using FluentValidation;
using NivaValidatorApi.Model.ECropReturnMessage;
using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.ECropReportMessage.Validator
{
	public class AgriculturalProductionUnitValidator :AbstractValidator<AgriculturalProductionUnit>
	{
		public AgriculturalProductionUnitValidator(OrganisationCode organisationCode, ReportTypeCode reportTypeCode)
		{
			RuleFor(x => x.SpecifiedCropPlot)
				.NotNull()
				.WithState(x1 => MessageCode.NullField)
				.ForEach(plot => plot.SetValidator(new SpecifiedCropPlotValidator(organisationCode, reportTypeCode)));
		}
	}
}
