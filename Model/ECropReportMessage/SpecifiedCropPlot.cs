﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// The specified crop plot.
	/// </summary>
	public partial class SpecifiedCropPlot
	{
		/// <summary>
		/// Area in square meters.
		/// </summary>
		[JsonProperty("AreaMeasure", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public double? AreaMeasure { get; set; }

		/// <summary>
		/// The ending date for this crop plot.
		/// </summary>
		[JsonProperty("EndDateTime", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string EndDateTime { get; set; }

		/// <summary>
		/// An array of field crops.
		/// </summary>
		[JsonProperty("GrownFieldCrop", Required = Required.Always)]
		public GrownFieldCrop[] GrownFieldCrop { get; set; }

		/// <summary>
		/// Identifier of this crop plot.
		/// </summary>
		[JsonProperty("ID", Required = Required.Always)]
		public string Id { get; set; }

		[JsonProperty("SpecifiedReferencedLocation", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public ReferencedLocation SpecifiedReferencedLocation { get; set; }

		/// <summary>
		/// The starting date for this crop plot.
		/// </summary>
		[JsonProperty("StartDateTime", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string StartDateTime { get; set; }
	}
}