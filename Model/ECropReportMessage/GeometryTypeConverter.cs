﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	internal class GeometryTypeConverter : JsonConverter
	{
		public override bool CanConvert(Type t) => t == typeof(GeometryType) || t == typeof(GeometryType?);

		public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null) return null;
			var value = serializer.Deserialize<string>(reader);
			switch (value)
			{
				case "MultiPolygon":
					return GeometryType.MultiPolygon;
				case "Point":
					return GeometryType.Point;
				case "Polygon":
					return GeometryType.Polygon;
			}
			throw new Exception("Cannot unmarshal type GeometryType");
		}

		public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
		{
			if (untypedValue == null)
			{
				serializer.Serialize(writer, null);
				return;
			}
			var value = (GeometryType)untypedValue;
			switch (value)
			{
				case GeometryType.MultiPolygon:
					serializer.Serialize(writer, "MultiPolygon");
					return;
				case GeometryType.Point:
					serializer.Serialize(writer, "Point");
					return;
				case GeometryType.Polygon:
					serializer.Serialize(writer, "Polygon");
					return;
			}
			throw new Exception("Cannot marshal type GeometryType");
		}

		public static readonly GeometryTypeConverter Singleton = new GeometryTypeConverter();
	}
}