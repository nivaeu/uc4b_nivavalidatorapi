﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	/// <summary>
	/// The geometry of this specific feature
	/// </summary>
	public abstract class Geometry
	{
		/// <summary>
		/// The geometry type for this geometry. Currently 'Polygon', but could be 'MultiPolygon',
		/// 'Point' and others. As per rfc7946 chapter 1.4, these strings are case sensitive.
		/// </summary>
		[JsonProperty("type", Required = Required.Always)]
		[JsonConverter(typeof(StringEnumConverter))]
		// ReSharper disable once InconsistentNaming
		public GeometryType type { get; set; }
	}

	public class PointGeometry : Geometry
	{
		[JsonProperty("coordinates", Required = Required.Always)]
		// ReSharper disable once InconsistentNaming
		public double[] coordinates { get; set; }

		public PointGeometry()
		{
			type = GeometryType.Point;
		}
	}

	public class PolygonGeometry : Geometry
	{
		[JsonProperty("coordinates", Required = Required.Always)]
		// ReSharper disable once InconsistentNaming
		public double[][][] coordinates { get; set; }

		public PolygonGeometry()
		{
			type = GeometryType.Polygon;
		}
	}
}