﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
    /// <summary>
    /// Plants or produce cultivated from a single botanical species or variety.
    /// </summary>
    public partial class SpecifiedBotanicalCrop
    {
        /// <summary>
        /// The code specifying the genus for this botanical crop.
        /// </summary>
        [JsonProperty("BotanicalGenusCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string BotanicalGenusCode { get; set; }

        /// <summary>
        /// The identifier for this botanical crop.
        /// </summary>
        [JsonProperty("BotanicalIdentificationID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string BotanicalIdentificationId { get; set; }

        /// <summary>
        /// The  botanical name,  expressed  as text for this botanical crop.
        /// </summary>
        [JsonProperty("BotanicalName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string BotanicalName { get; set; }

        /// <summary>
        /// The code specifying the species for this botanical crop.
        /// </summary>
        [JsonProperty("BotanicalSpeciesCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string BotanicalSpeciesCode { get; set; }

        /// <summary>
        /// The code specifying the purpose for this botanical crop.
        /// </summary>
        [JsonProperty("PurposeCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string PurposeCode { get; set; }

        /// <summary>
        /// An array of sub-divisions of cultivated plants within a species.
        /// </summary>
        [JsonProperty("SownCropSpeciesVariety", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public SownCropSpeciesVariety[] SownCropSpeciesVariety { get; set; }
    }
}