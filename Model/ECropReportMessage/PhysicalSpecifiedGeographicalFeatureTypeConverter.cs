﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	internal class PhysicalSpecifiedGeographicalFeatureTypeConverter : JsonConverter
	{
		public override bool CanConvert(Type t) => t == typeof(PhysicalSpecifiedGeographicalFeatureType) || t == typeof(PhysicalSpecifiedGeographicalFeatureType?);

		public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null) return null;
			var value = serializer.Deserialize<string>(reader);
			if (value == "Feature")
			{
				return PhysicalSpecifiedGeographicalFeatureType.Feature;
			}
			throw new Exception("Cannot unmarshal type PhysicalSpecifiedGeographicalFeatureType");
		}

		public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
		{
			if (untypedValue == null)
			{
				serializer.Serialize(writer, null);
				return;
			}
			var value = (PhysicalSpecifiedGeographicalFeatureType)untypedValue;
			if (value == PhysicalSpecifiedGeographicalFeatureType.Feature)
			{
				serializer.Serialize(writer, "Feature");
				return;
			}
			throw new Exception("Cannot marshal type PhysicalSpecifiedGeographicalFeatureType");
		}

		public static readonly PhysicalSpecifiedGeographicalFeatureTypeConverter Singleton = new PhysicalSpecifiedGeographicalFeatureTypeConverter();
	}
}