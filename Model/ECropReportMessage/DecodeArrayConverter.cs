﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NivaValidatorApi.Model.ECropReportMessage
{
	internal class DecodeArrayConverter : JsonConverter
	{
		public override bool CanConvert(Type t) => t == typeof(double[]);

		public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
		{
			reader.Read();
			var value = new List<double>();
			while (reader.TokenType != JsonToken.EndArray)
			{
				var converter = MinMaxValueCheckConverter.Singleton;
				var arrayItem = (double)converter.ReadJson(reader, typeof(double), null, serializer);
				value.Add(arrayItem);
				reader.Read();
			}
			return value.ToArray();
		}

		public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
		{
			var value = (double[])untypedValue;
			writer.WriteStartArray();
			foreach (var arrayItem in value)
			{
				var converter = MinMaxValueCheckConverter.Singleton;
				converter.WriteJson(writer, arrayItem, serializer);
			}
			writer.WriteEndArray();
			return;
		}

		public static readonly DecodeArrayConverter Singleton = new DecodeArrayConverter();
	}
}