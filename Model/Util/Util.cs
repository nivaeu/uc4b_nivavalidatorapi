﻿using NivaValidatorApi.Model.Util.Enums;

namespace NivaValidatorApi.Model.Util
{
	public static class NivaUtil
	{
		public static OrganisationCode GetOrganisationCode(string countryCode)
		{
			return countryCode.ToLower() switch
			{
				"dk" => Enums.OrganisationCode.DK,
				"nl" => Enums.OrganisationCode.NL,
				_ => Enums.OrganisationCode.Unknown
			};
		}

		public static ReportTypeCode GetReportTypeCode(string reportTypeCode)
		{
			return reportTypeCode.ToLower() switch
			{
				"scc" => Enums.ReportTypeCode.SCC,
				"smn" => Enums.ReportTypeCode.SMN,
				_ => Enums.ReportTypeCode.Unknown
			};
		}
	}
}
