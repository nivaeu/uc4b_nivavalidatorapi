﻿
// ReSharper disable InconsistentNaming

namespace NivaValidatorApi.Model.Util.Enums
{
	public enum OrganisationCode
	{
		DK,
		NL,
		Unknown
	}

	public enum ReportTypeCode
	{
		SCC,
		SMN,
		Unknown
	}
}
