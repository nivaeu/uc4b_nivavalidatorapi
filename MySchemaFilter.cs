using System;
using System.Linq;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using NivaValidatorApi.Model.ECropReportMessage;
using NivaValidatorApi.Model.Util.Enums;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace NivaValidatorApi
{
	public class MySchemaFilter :ISchemaFilter
	{
		public void Apply(OpenApiSchema schema, SchemaFilterContext context)
		{
			if (context.ApiModel.Type == typeof(CropReportDocument))
				schema.Example = CreateCropReportDocument();
			if (context.ApiModel.Type == typeof(AgriculturalProducerParty))
				schema.Example = CreateAgriculturalProducerParty();
			if (context.ApiModel.Type == typeof(AgriculturalProductionUnit))
				schema.Example = CreateAgriculturalProductionUnit();
			if (context.ApiModel.Type == typeof(SpecifiedCropPlot))
				schema.Example = CreateSpecifiedCropPlot();
			if (context.ApiModel.Type == typeof(ReferencedLocation))
				schema.Example = CreateReferencedLocation();
			if (context.ApiModel.Type == typeof(PhysicalSpecifiedGeographicalFeature))
				schema.Example = CreatePhysicalSpecifiedGeographicalFeature();
			if (context.ApiModel.Type == typeof(Geometry))
				schema.Example = CreateGeometry();
			if (context.ApiModel.Type == typeof(GrownFieldCrop))
				schema.Example = CreateGrownFieldCrop();
			if (context.ApiModel.Type == typeof(SpecifiedFieldCropMixtureConstituent))
				schema.Example = CreateSpecifiedFieldCropMixtureConstituent();
			if (context.ApiModel.Type == typeof(SpecifiedBotanicalCrop))
				schema.Example = CreateSpecifiedBotanicalCrop();
			if (context.ApiModel.Type == typeof(SownCropSpeciesVariety))
				schema.Example = CreateSownCropSpeciesVariety();
			if (context.ApiModel.Type == typeof(ApplicableCropProductionAgriculturalProcess))
				schema.Example = CreateApplicableCropProductionAgriculturalProcess();
			if (context.ApiModel.Type == typeof(AppliedSpecifiedAgriculturalApplication))
				schema.Example = CreateAppliedSpecifiedAgriculturalApplication();
			if (context.ApiModel.Type == typeof(SpecifiedProductBatch))
				schema.Example = CreateSpecifiedProductBatch();
			if (context.ApiModel.Type == typeof(AppliedSpecifiedAgriculturalApplicationRate))
				schema.Example = CreateAppliedSpecifiedAgriculturalApplicationRate();
		}

		#region Create Entity
		private GrownFieldCrop CreateGrownFieldCropEntity()
		{
			return new GrownFieldCrop
			{
				ClassificationCode = "233",
				ApplicableCropProductionAgriculturalProcess = new ApplicableCropProductionAgriculturalProcess[]
				{
					CreateApplicableCropProductionAgriculturalProcessEntity()
				},
				SpecifiedFieldCropMixtureConstituent = new SpecifiedFieldCropMixtureConstituent[]
				{
					CreateSpecifiedFieldCropMixtureConstituentEntity(),
					new SpecifiedFieldCropMixtureConstituent()
					{
						CropProportionPercent= 19.9,
						SpecifiedBotanicalCrop = new SpecifiedBotanicalCrop()
							{
								BotanicalIdentificationId= "5711",
								BotanicalSpeciesCode= "Grass field species",
								PurposeCode= "",
								BotanicalGenusCode= "",
								BotanicalName= "",
								SownCropSpeciesVariety= new SownCropSpeciesVariety []
								{
									new SownCropSpeciesVariety()
									{
										TypeCode= "",
										Description= ""
									}
								}
							}
					} 
				}
			};
		}

		private SpecifiedFieldCropMixtureConstituent CreateSpecifiedFieldCropMixtureConstituentEntity()
		{
			return new SpecifiedFieldCropMixtureConstituent
			{
				CropProportionPercent = 80.1,
				SpecifiedBotanicalCrop = CreateSpecifiedBotanicalCropEntity()
			};
		}

		private SpecifiedBotanicalCrop CreateSpecifiedBotanicalCropEntity()
		{
			return new SpecifiedBotanicalCrop
			{
				BotanicalIdentificationId = "5799",
				BotanicalSpeciesCode = "Spring wheat",
				PurposeCode = "",
				BotanicalGenusCode = "",
				BotanicalName = "",
				SownCropSpeciesVariety =new SownCropSpeciesVariety[]
				{
					CreateSownCropSpeciesVarietyEntity()
				}
			};
		}

		private SownCropSpeciesVariety CreateSownCropSpeciesVarietyEntity()
		{
			return new SownCropSpeciesVariety
			{
				TypeCode = "",
				Description="",
			};
		}

		private ApplicableCropProductionAgriculturalProcess CreateApplicableCropProductionAgriculturalProcessEntity()
		{
			return new ApplicableCropProductionAgriculturalProcess
			{
				TypeCode="301",
				SubordinateTypeCode = "1013",
				ActualStartDateTime = DateTime.Now.AddHours(-2).ToString("s"),
				ActualEndDateTime = DateTime.Now.ToString("s"),
				AppliedSpecifiedAgriculturalApplication = new AppliedSpecifiedAgriculturalApplication[]
				{
					CreateAppliedSpecifiedAgriculturalApplicationEntity()
				}
			};
		}

		private AppliedSpecifiedAgriculturalApplication CreateAppliedSpecifiedAgriculturalApplicationEntity()
		{
			return new AppliedSpecifiedAgriculturalApplication
			{
				Id = "1",
				AppliedSpecifiedAgriculturalApplicationRate = new AppliedSpecifiedAgriculturalApplicationRate[]
				{
					CreateAppliedSpecifiedAgriculturalApplicationRateEntity()
				}
				,SpecifiedProductBatch = new SpecifiedProductBatch[]
				{
					CreateSpecifiedProductBatchEntity()
				}

			};
		}

		private SpecifiedProductBatch CreateSpecifiedProductBatchEntity()
		{
			return new SpecifiedProductBatch
			{
				Id= "1195068",
				ProductName= "NS 26-15",
				TypeCode="S",
				SizeMeasure = 0,
				WeightMeasure=250,
				UnitQuantity=0,
				AppliedTreatment="Some treatment text"
			};
		}

		private AppliedSpecifiedAgriculturalApplicationRate CreateAppliedSpecifiedAgriculturalApplicationRateEntity()
		{
			return new AppliedSpecifiedAgriculturalApplicationRate
			{
				AppliedQuantity=300,
				AppliedQuantityUnit="kg/ha",
				AppliedReferencedLocation = new ReferencedLocation[]
				{
					CreateReferencedLocationEntity()
				}
			};
		}

		private PolygonGeometry CreateGeometryEntity()
		{
			return new PolygonGeometry
			{
				coordinates = new double[][][]
				{
					new double[][]
					{
						new double[]{ 5.41412794564089, 52.148451591648602},
						new double[]{ 5.41784357231444, 52.154784683175897 },
						new double[]{5.4232748279405, 52.157515523436402}, 
						new double[]{5.44190101146912, 52.161515857321398}, 
						new double[]{5.45934006430159, 52.152914781734403}, 
						new double[]{5.4767474810069, 52.152217560150099}, 
						new double[]{5.48524781316738, 52.149059841944698}, 
						new double[]{5.49898246813153, 52.147393356875099}, 
						new double[]{5.49730624313368, 52.141443501164503}, 
						new double[]{5.51407939060404, 52.135923062782297},
						new double[]{5.51192753027101, 52.132104647718002},
						new double[]{5.49791305937211, 52.130282891822198},
						new double[]{5.49675907542549, 52.124814284307803},
						new double[]{5.49469492804353, 52.124697887324601},
						new double[]{5.49691768162249, 52.119557486966002},
						new double[]{5.5053539453497, 52.119750446627997},
						new double[]{5.50375599233665, 52.116585633047798},
						new double[]{5.50754867283719, 52.115644548930597},
						new double[]{5.50770206758915, 52.113896963649999},
						new double[]{5.50394963830627, 52.113720283830297},
						new double[]{5.4989165192663, 52.116017637139898},
						new double[]{5.48644708858844, 52.115359736284802},
						new double[]{5.4790790316083, 52.117426180146403},
						new double[]{5.47582548628898, 52.112071103593401},
						new double[]{5.47603713431143, 52.105012153653398},
						new double[]{5.47370706432387, 52.104759628267502},
						new double[]{5.47198731898116, 52.101455683546298},
						new double[]{5.46751649575624, 52.1004002579757},
						new double[]{5.4578659625346, 52.1053692534554},
						new double[]{5.44380666820611, 52.107074562152498},
						new double[]{5.43793794535015, 52.099743999985002},
						new double[]{5.4296290238713, 52.0959104650859},
						new double[]{5.42104052224432, 52.096901227657398},
						new double[]{5.41015851335331, 52.103263259459702},
						new double[]{5.40452086680621, 52.104106397547604},
						new double[]{5.40133209475129, 52.089333533202499},
						new double[]{5.37800991009383, 52.090471605560502},
						new double[]{5.33587431830306, 52.095969827017299},
						new double[]{5.31091980123712, 52.108384387218003},
						new double[]{5.3340364595374, 52.1204148544251},
						new double[]{5.35563520932537, 52.136829573691202},
						new double[]{5.37438878713075, 52.130992808132703},
						new double[]{5.3980746995931, 52.133547807457298},
						new double[]{5.40756324878673, 52.141959811032102},
						new double[]{5.40939097693997, 52.140387423581899},
						new double[]{5.40831834228932, 52.142588213350699},
						new double[]{5.41412794564089, 52.148451591648602}
					}
				}
			};
		}

		private PhysicalSpecifiedGeographicalFeature CreatePhysicalSpecifiedGeographicalFeatureEntity()
		{
			return new PhysicalSpecifiedGeographicalFeature
			{
				Type=PhysicalSpecifiedGeographicalFeatureType.Feature,
				Geometry = CreateGeometryEntity()
			};
		}

		private ReferencedLocation CreateReferencedLocationEntity()
		{
			return new ReferencedLocation
			{
				Id="9999ZZ1",
				Name="Location 1",
				TypeCode="CAP",
				Description="Location 1",
				ReferenceTypeCode="F",
				PhysicalSpecifiedGeographicalFeature = CreatePhysicalSpecifiedGeographicalFeatureEntity(),
			};
		}

		private SpecifiedCropPlot CreateSpecifiedCropPlotEntity()
		{
			return new SpecifiedCropPlot
			{
				Id="1",
				StartDateTime=new DateTime(2019,5,1).ToString("s"),
				EndDateTime=new DateTime(2020,5, 1).ToString("s"),
				AreaMeasure=9873.2501,
				GrownFieldCrop = new GrownFieldCrop[]
				{
					CreateGrownFieldCropEntity(),
					new GrownFieldCrop()
					{
						ClassificationCode = "234",
						ApplicableCropProductionAgriculturalProcess = new ApplicableCropProductionAgriculturalProcess[]
						{
							CreateApplicableCropProductionAgriculturalProcessEntity()
						},
						SpecifiedFieldCropMixtureConstituent = new SpecifiedFieldCropMixtureConstituent[]
						{
							CreateSpecifiedFieldCropMixtureConstituentEntity(),
							CreateSpecifiedFieldCropMixtureConstituentEntity()
						}
					}
				},
				SpecifiedReferencedLocation = CreateReferencedLocationEntity()
			};
		}

		private AgriculturalProductionUnit CreateAgriculturalProductionUnitEntity()
		{
			return new AgriculturalProductionUnit
			{
				Id = "UBN-123456789",
				TypeCode = "token",
				Name = "Testfarm - Buitenweg 1 - 9999 ZZ - Testdorp - Nederland",
				SpecifiedCropPlot = new SpecifiedCropPlot[]
				{
					CreateSpecifiedCropPlotEntity(),
					new SpecifiedCropPlot()
					{
						Id = "2",
						StartDateTime = new DateTime(2019, 1, 5).ToString("s"),
						EndDateTime = new DateTime(2020, 4, 30).ToString("s"),
						AreaMeasure = 9873,
						GrownFieldCrop = new GrownFieldCrop[]
						{},
						SpecifiedReferencedLocation = new ReferencedLocation()
						{
							Id = "9999ZZ2",
							Name = "Location 2",
							TypeCode = "CAP",
							Description = "Location 2",
							ReferenceTypeCode = "F",
							PhysicalSpecifiedGeographicalFeature = new PhysicalSpecifiedGeographicalFeature()
							{
								Type=PhysicalSpecifiedGeographicalFeatureType.Feature,
								Geometry = CreateGeometryEntity()
							}
						}
					}
				}
			};
		}

		private AgriculturalProducerParty CreateAgriculturalProducerPartyEntity()
		{
			return new AgriculturalProducerParty
			{
				Id="BRS-123456789",
				Name="J. Smit",
				Description="J. Smit - Buitenweg 1 - 9999 ZZ - Testdorp - Nederland",
				ClassificationCode="A1.1.1",
				AgriculturalProductionUnit = CreateAgriculturalProductionUnitEntity()
			};
		}

		private CropReportDocument CreateCropReportDocumentEntity()
		{
			return new CropReportDocument
			{
				Id="1234567890",
				ReportCountNumeric = 1,
				Description="eCrop report message",
				IssueDateTime = DateTime.Now.ToString("s"),
				TypeCode=ReportTypeCode.SMN.ToString(),
				CopyIndicator = false,
				ControlRequirementIndicator=false,
				PurposeCode="S",
				LineCountNumeric = 440,
				Information="Some random information text",
				StatusCode="F",
				SequenceId="1",
				SenderSpecifiedParty = new SpecifiedParty()
				{
					Id="BRS-123456789",
					Name="J. Smit",
					CountryId="NL"
				},
				RecipientSpecifiedParty = new SpecifiedParty()
				{
					Id="BRS-99999999",
					Name="Rijksdienst voor Ondernemend Nederland",
					CountryId="NL"
				}
			};
		}

		private SpecifiedParty CreateSpecifiedPartyEntity()
		{
			return new SpecifiedParty()
			{
				Id = "BRS-99999999",
				Name = "Rijksdienst voor Ondernemend Nederland",
				CountryId = "528"
			};
		}

		#endregion

		#region Create OpenApi
		private IOpenApiAny CreateGrownFieldCrop(GrownFieldCrop entity=null)
		{
			if (entity == null)
				entity = CreateGrownFieldCropEntity();

			var specifiedFieldCropMixtureConstituent = new OpenApiArray();
			specifiedFieldCropMixtureConstituent.AddRange(entity.SpecifiedFieldCropMixtureConstituent
				.Select(CreateSpecifiedFieldCropMixtureConstituent));
			var applicableCropProductionAgriculturalProcess = new OpenApiArray();
			applicableCropProductionAgriculturalProcess.AddRange(entity.ApplicableCropProductionAgriculturalProcess
				.Select(CreateApplicableCropProductionAgriculturalProcess));

			return new OpenApiObject
			{
				["ClassificationCode"] = new OpenApiString(entity.ClassificationCode),
				["ApplicableCropProductionAgriculturalProcess"] = applicableCropProductionAgriculturalProcess,
				["SpecifiedFieldCropMixtureConstituent"] = specifiedFieldCropMixtureConstituent
			};
		}

		private IOpenApiAny CreateSpecifiedFieldCropMixtureConstituent(SpecifiedFieldCropMixtureConstituent entity=null)
		{
			if (entity == null)
				entity = CreateSpecifiedFieldCropMixtureConstituentEntity();
			return new OpenApiObject
			{
				//todo
				["CropProportionPercent"] = new OpenApiDouble(Math.Round(entity.CropProportionPercent)),
				["SpecifiedBotanicalCrop"] = entity.SpecifiedBotanicalCrop != null?CreateSpecifiedBotanicalCrop(entity.SpecifiedBotanicalCrop):null
			};
		}

		private IOpenApiAny CreateSpecifiedBotanicalCrop(SpecifiedBotanicalCrop entity=null)
		{
			if (entity == null)
				entity = CreateSpecifiedBotanicalCropEntity();

			var sownCropSpeciesVariety = new OpenApiArray();
			sownCropSpeciesVariety.AddRange(entity.SownCropSpeciesVariety.Select(CreateSownCropSpeciesVariety));
			return new OpenApiObject
			{
				["BotanicalIdentificationID"] = new OpenApiString(entity.BotanicalIdentificationId),
				["BotanicalSpeciesCode"] = new OpenApiString(entity.BotanicalSpeciesCode),
				["PurposeCode"] = new OpenApiString(entity.PurposeCode),
				["BotanicalGenusCode"] = new OpenApiString(entity.BotanicalGenusCode),
				["BotanicalName"] = new OpenApiString(entity.BotanicalName),
				["SownCropSpeciesVariety"] =sownCropSpeciesVariety
			};
		}

		private IOpenApiAny CreateSownCropSpeciesVariety(SownCropSpeciesVariety entity=null)
		{
			if (entity == null)
				entity = CreateSownCropSpeciesVarietyEntity();
			return new OpenApiObject
			{
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				["Description"] = new OpenApiString(entity.Description)
			};
		}

		private IOpenApiAny CreateApplicableCropProductionAgriculturalProcess(ApplicableCropProductionAgriculturalProcess entity=null)
		{
			if (entity == null)
				entity = CreateApplicableCropProductionAgriculturalProcessEntity();

			var appliedSpecifiedAgriculturalApplication = new OpenApiArray();
			appliedSpecifiedAgriculturalApplication.AddRange(
				entity.AppliedSpecifiedAgriculturalApplication.Select(CreateAppliedSpecifiedAgriculturalApplication));
			return new OpenApiObject
			{
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				["SubordinateTypeCode"] = new OpenApiString(entity.SubordinateTypeCode),
				["ActualStartDateTime"] = new OpenApiString(entity.ActualStartDateTime),
				["ActualEndDateTime"] = new OpenApiString(entity.ActualEndDateTime),
				["AppliedSpecifiedAgriculturalApplication"] = appliedSpecifiedAgriculturalApplication,
			};
		}

		private IOpenApiAny CreateAppliedSpecifiedAgriculturalApplication(AppliedSpecifiedAgriculturalApplication entity=null)
		{
			if (entity == null)
				entity = CreateAppliedSpecifiedAgriculturalApplicationEntity();

			var appliedSpecifiedAgriculturalApplicationRate = new OpenApiArray();
			appliedSpecifiedAgriculturalApplicationRate.AddRange(entity.AppliedSpecifiedAgriculturalApplicationRate.Select(CreateAppliedSpecifiedAgriculturalApplicationRate));
			var specifiedProductBatch = new OpenApiArray();
			specifiedProductBatch.AddRange(entity.SpecifiedProductBatch.Select(CreateSpecifiedProductBatch));
			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["AppliedSpecifiedAgriculturalApplicationRate"]=appliedSpecifiedAgriculturalApplicationRate,
				["SpecifiedProductBatch"] = specifiedProductBatch,
			};
		}

		private IOpenApiAny CreateSpecifiedProductBatch(SpecifiedProductBatch entity=null)
		{
			if (entity == null)
				entity = CreateSpecifiedProductBatchEntity();

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["ProductName"] = new OpenApiString(entity.ProductName),
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				//todo
				["SizeMeasure"] = entity.SizeMeasure.HasValue?new OpenApiDouble(Math.Round(entity.SizeMeasure.Value)):null,
				["WeightMeasure"] = entity.WeightMeasure.HasValue?new OpenApiDouble(Math.Round(entity.WeightMeasure.Value)):null,
				["UnitQuantity"] = entity.UnitQuantity.HasValue?new OpenApiDouble(Math.Round(entity.UnitQuantity.Value)):null,
				["AppliedTreatment"] = new OpenApiString(entity.AppliedTreatment)
			};
		}

		private IOpenApiAny CreateAppliedSpecifiedAgriculturalApplicationRate(AppliedSpecifiedAgriculturalApplicationRate entity=null)
		{
			if (entity == null)
				entity = CreateAppliedSpecifiedAgriculturalApplicationRateEntity();

			var appliedReferencedLocation = new OpenApiArray();
			appliedReferencedLocation.AddRange(entity.AppliedReferencedLocation.Select(CreateReferencedLocation));
			return new OpenApiObject
			{
				//todo
				["AppliedQuantity"] = new OpenApiDouble(Math.Round(entity.AppliedQuantity)),
				["AppliedQuantityUnit"] = new OpenApiString(entity.AppliedQuantityUnit),
				["AppliedReferencedLocation"] = appliedReferencedLocation
			};
		}

		private IOpenApiAny CreateGeometry(Geometry entity=null)
		{
			if (entity == null)
				entity = CreateGeometryEntity();

			OpenApiArray coordinates = null;
			if (entity is PointGeometry pointGeometry)
			{
				coordinates = new OpenApiArray();
				coordinates.AddRange(pointGeometry.coordinates.Select(polygon =>
				{
					var polygonArray = new OpenApiArray();
					//todo
					return polygonArray;
				}));
			} else if (entity is PolygonGeometry polygonGeometry)
			{
				coordinates = new OpenApiArray();
				coordinates.AddRange(polygonGeometry.coordinates.Select(polygon =>
				{
					var polygonArray = new OpenApiArray();
					//polygonArray.AddRange(
					//polygon.Select(coordinate =>
					//{
					//	var coordinateArray = new OpenApiArray();
					//	//todo
					//	//coordinateArray.AddRange(coordinate.Select(xy => new OpenApiInteger((int)xy)));
					//	return coordinateArray;
					//}));
					return polygonArray;
				}));
			}

			return new OpenApiObject
			{
				["type"] = new OpenApiString(entity.type.ToString()),
				["coordinates"] = coordinates
			};
		}

		private IOpenApiAny CreatePhysicalSpecifiedGeographicalFeature(PhysicalSpecifiedGeographicalFeature entity=null)
		{
			if (entity == null)
				entity = CreatePhysicalSpecifiedGeographicalFeatureEntity();
			return new OpenApiObject
			{
				["type"] = new OpenApiString(entity.Type.ToString()),
				//todo
				["geometry"] = entity.Geometry!=null?CreateGeometry(entity.Geometry):null
			};
		}

		private IOpenApiAny CreateReferencedLocation(ReferencedLocation entity=null)
		{
			if (entity == null)
				entity = CreateReferencedLocationEntity();

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["Name"] = new OpenApiString(entity.Name),
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				["Description"] = new OpenApiString(entity.Description),
				["ReferenceTypeCode"] = new OpenApiString(entity.ReferenceTypeCode),
				["PhysicalSpecifiedGeographicalFeature"] = entity.PhysicalSpecifiedGeographicalFeature != null
					? CreatePhysicalSpecifiedGeographicalFeature(entity.PhysicalSpecifiedGeographicalFeature)
					: null
			};
		}

		private IOpenApiAny CreateSpecifiedCropPlot(SpecifiedCropPlot entity=null)
		{
			if (entity == null)
				entity = CreateSpecifiedCropPlotEntity();

			var grownFieldCrop = new OpenApiArray();
			grownFieldCrop.AddRange(entity.GrownFieldCrop.Select(CreateGrownFieldCrop));

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["StartDateTime"] = new OpenApiString(entity.StartDateTime),
				["EndDateTime"] = new OpenApiString(entity.EndDateTime),
				//todo
				["AreaMeasure"] = entity.AreaMeasure.HasValue?new OpenApiDouble(Math.Round(entity.AreaMeasure.Value)):null,
				["SpecifiedReferencedLocation"] = entity.SpecifiedReferencedLocation != null
					? CreateReferencedLocation(entity.SpecifiedReferencedLocation)
					: null,
				["GrownFieldCrop"] = grownFieldCrop
			};
		}

		private IOpenApiAny CreateAgriculturalProductionUnit(AgriculturalProductionUnit entity=null)
		{
			if (entity == null)
				entity = CreateAgriculturalProductionUnitEntity();

			var specifiedCropPlot = new OpenApiArray();
			specifiedCropPlot.AddRange(entity.SpecifiedCropPlot.Select(CreateSpecifiedCropPlot));

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				["Name"] = new OpenApiString(entity.Name),
				["SpecifiedCropPlot"] = specifiedCropPlot
			};
		}

		private IOpenApiAny CreateAgriculturalProducerParty(AgriculturalProducerParty entity=null)
		{
			if (entity == null)
				entity = CreateAgriculturalProducerPartyEntity();

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["Name"] = new OpenApiString(entity.Name),
				["Description"] = new OpenApiString(entity.Description),
				["ClassificationCode"] = new OpenApiString(entity.ClassificationCode),
				["AgriculturalProductionUnit"] = entity.AgriculturalProductionUnit != null
					? CreateAgriculturalProductionUnit(entity.AgriculturalProductionUnit)
					: null
			};
		}

		private IOpenApiAny CreateCropReportDocument(CropReportDocument entity=null)
		{
			if (entity == null)
				entity = CreateCropReportDocumentEntity();

			return new OpenApiObject
			{
				["ID"] = new OpenApiString(entity.Id),
				["ReportCountNumeric"] = entity.ReportCountNumeric.HasValue?new OpenApiLong(entity.ReportCountNumeric.Value):null,
				["Description"] = new OpenApiString(entity.Description),
				["IssueDateTime"] = new OpenApiString(entity.IssueDateTime),
				["TypeCode"] = new OpenApiString(entity.TypeCode),
				["CopyIndicator"] = new OpenApiBoolean(entity.CopyIndicator),
				["ControlRequirementIndicator"] = new OpenApiBoolean(entity.ControlRequirementIndicator),
				["PurposeCode"] = new OpenApiString(entity.PurposeCode),
				["LineCountNumeric"] = entity.LineCountNumeric.HasValue?new OpenApiLong(entity.LineCountNumeric.Value):null,
				["Information"] = new OpenApiString(entity.Information),
				["StatusCode"] = new OpenApiString(entity.StatusCode),
				["SequenceID"] = new OpenApiString(entity.SequenceId),
				["SenderSpecifiedParty"] = entity.SenderSpecifiedParty!=null?CreateSpecifiedParty(entity.SenderSpecifiedParty):null,
				["RecipientSpecifiedParty"] = entity.RecipientSpecifiedParty!=null?CreateSpecifiedParty(entity.RecipientSpecifiedParty):null
				
			};
		}

		private IOpenApiAny CreateSpecifiedParty(SpecifiedParty entity=null)
		{
			if (entity == null)
				entity = CreateSpecifiedPartyEntity();
			return new OpenApiObject()
			{
				["ID"] = new OpenApiString(entity.Id),
				["Name"] = new OpenApiString(entity.Name),
				["CountryID"] = new OpenApiString(entity.CountryId)
			};
		}

		#endregion
	}
}